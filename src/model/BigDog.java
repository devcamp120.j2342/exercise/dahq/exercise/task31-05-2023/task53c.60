package model;

public class BigDog extends Dog {

    public BigDog(String name) {
        super(name);
    }

    @Override
    public void Greet() {
        System.out.println("Woow");
    }

    @Override
    public void Greet(Dog dog) {
        System.out.println("Wooooow");
    }

    public void Greet(BigDog bigDog) {
        System.out.println("Wooooooooooooooooooooow");
    }

}
