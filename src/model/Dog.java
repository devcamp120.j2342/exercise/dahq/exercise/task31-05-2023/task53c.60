package model;

public class Dog extends Animal {

    public Dog(String name) {
        super(name);
    }

    @Override
    public void Greet() {
        System.out.println("Woof");
    }

    public void Greet(Dog dog1) {
        System.out.println("Woooof");
    }

}
