package model;

public class Cat extends Animal {

    public Cat(String name) {
        super(name);
    }

    @Override
    public void Greet() {
        System.out.println("Moew");
    }

}
