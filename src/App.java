import model.Animal;
import model.BigDog;
import model.Cat;
import model.Dog;

public class App {
    public static void main(String[] args) throws Exception {
        Dog dog = new Dog("Buul");
        dog.Greet();
        Cat cat = new Cat("Cash");
        cat.Greet();
        BigDog bigDog = new BigDog("Begge");
        bigDog.Greet();

    }
}
